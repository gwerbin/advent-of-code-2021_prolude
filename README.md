# Prolude

A library to solve problems.

# AOC Template

A template for [advent of code][AOC] is available in [`aoc-template`](aoc-template/). Follow the instructions in the
[README](aoc-template/README.md) to get started with `prolude`.

# Feature requests


- [x] An n-dimensional array API [Prolude.NGrid](src/Prolude/Data/NGrid.idr) 
- [ ] more parsing function for CSV-like files #1
- [ ] some `Interpolation` instances so that you don't have to write them #2
- [ ] An API for interpreters and stack machines #3
- [ ] More tests #4	
- [ ] Make `prolude` a drop-in replacement for `prelude`, `base` and `contrib` #5

[AOC]: https://adventofcode.com/
