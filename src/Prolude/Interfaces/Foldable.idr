module Prolude.Interfaces.Foldable

public export
size : Foldable t => t a -> Nat
size = count (const True)

