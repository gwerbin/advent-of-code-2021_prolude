module Prolude.Data.Queue

import Data.List
import Prolude.Data.List
import Prolude.Data.Vect
import Prolude.Data.Nat
import Prolude.Data.Maybe
import Data.Vect

export
record Queue (a : Type) where
  constructor MkQueue
  front : List a 
  back : List a

export
Functor Queue where
  map f (MkQueue fr bk) = MkQueue (map f fr) (map f bk)

export
Applicative Queue where
  pure x = MkQueue (pure x) []
  (MkQueue ffr fbk) <*> (MkQueue afr abk) = MkQueue (ffr <*> afr) (fbk <*> abk)

export
push : a -> Queue a -> Queue a
push x (MkQueue front back) = MkQueue (x :: front) back

export
pop : Queue a -> Maybe (Queue a)
pop (MkQueue front []) = Nothing
pop (MkQueue front (x :: xs)) = Just (MkQueue front xs)

export
popElem : Queue a -> Maybe (a , Queue a)
popElem (MkQueue front []) = Nothing
popElem (MkQueue front (x :: xs)) = Just (x, MkQueue front xs)

namespace Queue
  export
  fromList : List a -> Queue a
  fromList xs = let (front, back) = half xs in MkQueue front (reverse back)
  
  export
  toList : Queue a -> List a
  toList qs = qs.front ++ reverse qs.back

export
Monad Queue where
  qs >>= fn = fromList (toList qs >>= toList . fn) 

--------------------------------------------------------------------------------
-- Bidirectional sized queue
--------------------------------------------------------------------------------


||| Bidirectional queue
||| structure:     [ x1 , x2, .. , xn ]
||| The following operations are O(1) (amortized):
|||   - popFront : x1 [x2, x3, .. , xn ]
|||   - popBack : xn [x1, x2, .., xn-1 ]
|||   - pushFront x0 : [x0, x1, .. , xn ]
|||   - pushBack x0 : [x1, .. , xn, x0]
export
record SizedQueue (n : Nat) (a : Type) where
  constructor MkSizedQueue
  {s1 : Nat}
  {s2 : Nat}
  {auto prf : s1 + s2 = n}
  front : Vect s1 a
  back : Vect s2 a

||| I thought this used to be in the standard library...
succInjective : (0 l : Nat) -> (0 r : Nat) -> S l = S r -> l = r
succInjective _ _ Refl = Refl

||| Push an element onto the front of the queue
export
pushFront : a -> SizedQueue n a -> SizedQueue (S n) a
pushFront x (MkSizedQueue {s1} {s2} {prf} fr bk) = 
  MkSizedQueue {s1 = S s1} {s2} {prf = cong S prf} (x :: fr) bk

||| Push an element at the back of the queue
export
pushBack : a -> SizedQueue n a -> SizedQueue (S n) a
pushBack x (MkSizedQueue {s1} {s2} {prf} fr bk) = 
  MkSizedQueue {s1 = s1} {s2 = S s2} {prf = sym (plusSuccRightSucc s1 s2) `trans` (cong S prf)} fr (x :: bk)

-- export
-- popBack : SizedQueue (S n) a -> (SizedQueue n a)
-- popBack (MkSizedQueue {s1} {s2=Z} {prf} front []) with (sym (plusZeroRightNeutral s1) `trans` prf )
--   popBack (MkSizedQueue {s1 = S n} {s2=Z} {prf} front []) | Refl = 
--     MkSizedQueue [] (tail (reverse front))
-- popBack (MkSizedQueue front {s1} {s2 = S n} {prf} (x :: xs)) = 
--   let updatedProof = succInjective _ _ (plusSuccRightSucc s1 _  `trans` prf)
--    in MkSizedQueue {prf = updatedProof} front xs

||| Pop an element from the back of the queue
export
popElemBack : SizedQueue (S n) a -> (a , SizedQueue n a)
popElemBack (MkSizedQueue {s1} {s2=Z} {prf} front []) with (sym (plusZeroRightNeutral s1) `trans` prf )
  popElemBack (MkSizedQueue {s1 = S n} {s2=Z} {prf} front []) | Refl = 
    (head front, MkSizedQueue [] (tail (reverse front)))
popElemBack (MkSizedQueue front {s1} {s2 = S n} {prf} (x :: xs)) = 
  let updatedProof = succInjective _ _ (plusSuccRightSucc s1 _  `trans` prf)
   in (x, MkSizedQueue {prf = updatedProof} front xs)

||| Discard the last element of the queue
export
popBack : SizedQueue (S n) a -> (SizedQueue n a)
popBack = snd . popElemBack

||| Pop an element from the front of the queue
export
popElemFront : SizedQueue (S n) a -> (a , SizedQueue n a)
popElemFront (MkSizedQueue {s1=Z} {s2 = S n} {prf=Refl} [] back) = 
  let (x :: xs) = reverse back in 
      (x, MkSizedQueue {prf = plusZeroRightNeutral n} xs [])
popElemFront (MkSizedQueue {s1=S n} {s2} {prf} (x :: xs) back) = 
  (x, MkSizedQueue {prf = succInjective _ _ prf} xs back)

||| Discard the first element of the queue
export
popFront : SizedQueue (S n) a -> SizedQueue n a
popFront = snd . popElemFront

namespace SizedQueue
  export
  fromList : (ls : List a) -> (SizedQueue (length ls) a)
  fromList ls = rewrite sym (plusZeroRightNeutral (length ls)) in 
                        MkSizedQueue (Vect.fromList ls) []
  
  export
  toSized : (n : Nat) -> Queue a -> Maybe (SizedQueue n a)
  toSized n (MkQueue front back) = 
    let (n1 ** fs) = sizedList front
        (n2 ** bs) = sizedList back in
        do prf <- partialEq (isSum n1 n2 n)
           pure (MkSizedQueue {prf} fs bs)
  
  export
  toList : SizedQueue n a -> List a
  toList (MkSizedQueue front back) = toList front ++ reverse (toList back)

export
reverse : SizedQueue n a -> SizedQueue n a
reverse (MkSizedQueue {s1} {s2} {prf} front back) = MkSizedQueue {prf = plusCommutative s2 s1 `trans` prf} (reverse back) (reverse front)
