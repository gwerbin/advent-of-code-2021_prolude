module Prolude.Data.Group

public export
interface Monoid m => Group m where
  constructor MkGroup
  inverse : m -> m
