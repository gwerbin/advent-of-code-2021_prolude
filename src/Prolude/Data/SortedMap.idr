module Prolude.Data.SortedMap

import Data.SortedMap
import Data.Maybe

export
insertIfMissing : Eq a => (a , b) -> SortedMap a b ->  SortedMap a b
insertIfMissing (key, value) map =
  case lookup key map of
       Nothing => insert key value map
       Just v => map

export
update : key -> (updateFn : value -> value) -> SortedMap key value -> SortedMap key value
update key updateFn smap =
  let value = map updateFn $ lookup key smap in
      fromMaybe smap $ map (\v => insert key v smap) value
