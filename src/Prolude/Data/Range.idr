module Prolude.Data.Range

||| A bounded range with inclusive upper bound
public export
record Bounds (a : Type) where
  constructor MkBounds
  lower : a
  upper : a

export
inBounds : Ord a => a -> Bounds a -> Bool
inBounds x (MkBounds y z) = y <= x && x <= z
