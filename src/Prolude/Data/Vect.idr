module Prolude.Data.Vect

import public Data.Vect
import Data.List 

export
max : Ord a => (ls : Vect (S n) a) -> a
max [x] = x
max (x :: (y :: xs)) = Prelude.max x (Vect.max (y :: xs))

public export
sizedList : List a -> (n ** Vect n a)
sizedList xs = (length xs ** fromList xs)

export
enumerated : Num i => Vect n a -> Vect n (i, a)
enumerated x = enumerateAcc x 0
  where
    enumerateAcc : Vect m a -> i -> Vect m (i, a)
    enumerateAcc [] x = []
    enumerateAcc (y :: xs) x = (x, y) :: enumerateAcc xs (x + 1)

export
sortBy : (a -> a -> Ordering) -> Vect n a -> Vect n a
sortBy f xs = believe_me $ Vect.fromList $ sortBy f (toList xs)

export
indexNat : Vect n a -> Nat -> Maybe a
indexNat [] k = Nothing
indexNat (x :: xs) 0 = Just x
indexNat (x :: xs) (S k) = indexNat xs k
