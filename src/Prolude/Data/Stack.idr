module Prolude.Data.Stack

export
record Stack (a : Type) where
  constructor MkStack
  unstack : List a

export
Functor Stack where
  map f (MkStack ls) = MkStack (map f ls)

export
Foldable Stack where
  foldr f acc = foldr f acc . unstack

export
Traversable Stack where
  traverse f = map MkStack . traverse f . unstack

%inline
export
fromList : List a -> Stack a
fromList = MkStack

%inline
export
toList : Stack a -> List a
toList  = unstack

export
pop : Stack a -> (Maybe a, Stack a)
pop (MkStack []) = (Nothing, MkStack [])
pop (MkStack (x :: xs)) = (Just x, MkStack xs)

export
popElem : Stack a -> Maybe a
popElem = fst . pop

export
push : a -> Stack a -> Stack a
push v (MkStack ls) = MkStack (v :: ls)


