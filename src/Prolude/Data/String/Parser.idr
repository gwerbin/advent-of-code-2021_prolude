module Prolude.Data.String.Parser

import Data.String.Parser

parseAll : Parser a -> String -> Either String a
parseAll p = map fst . parse (p <* eos)

parseArray : Parser a -> String -> Either String (List a)
parseArray p = map fst . parse (commaSep (lexeme p) <* eos)
