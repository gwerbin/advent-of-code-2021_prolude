module Prolude.Proofs

public export
transport : a = b -> a -> b
transport Refl x = x
